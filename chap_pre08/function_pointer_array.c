#include<stdio.h>

typedef int (*operation) (int, int);

int add(int a, int b) { return a+b; }
int sub(int a, int b) { return a-b; }

operation operations[128] = {NULL};

void initialize() {
  operations['+'] = add;
  operations['-'] = sub;
}

int evaluate(char opCode, int a, int b) {
  operation op = operations[opCode];
  return op(a, b);
}

int main() {
  initialize();
  printf("%d\n", evaluate('+', 5, 6));
  printf("%d\n", evaluate('-', 5, 6));
}