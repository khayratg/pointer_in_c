#include<stdio.h>

int main() {
  char firstName[8] = "1234567";
  char middleName[8] = "1234567";
  char lastName[8] = "1234567";

  printf("firstName  %p %p %s\n", firstName, &firstName[1], firstName);
  printf("middleName %p %p %s\n", middleName, &middleName[1], middleName);
  printf("lastName   %p %p %s\n", lastName, &lastName[1], lastName);

  middleName[-2] = 'Y';
  middleName[0] = 'X';
  middleName[10] = 'Z';

  printf("\n");

  printf("firstName  %p %p %s\n", firstName, &firstName[1], firstName);
  printf("middleName %p %p %s\n", middleName, &middleName[1], middleName);
  printf("lastName   %p %p %s\n", lastName, &lastName[1], lastName);

  printf("lastName %p %c\n", &lastName[6], lastName[6]);
  printf("firstName %p %c\n", &firstName[2], firstName[2]);
}
