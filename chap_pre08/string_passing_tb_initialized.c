#include<stdio.h>
#include<stdlib.h>
#include<string.h>

char* format(char* buffer, size_t size, char* name, size_t quantity, size_t weight) {
  char* formatString = "name: %s, quantity: %lu, weight: %lu.";

  if (buffer == NULL) {
    size = strlen(formatString) + strlen(name) + 10 + 10 + 1;
    buffer = (char*) malloc(size);
  }

  snprintf(buffer, size, formatString, name, quantity, weight);

  return buffer;
}

int main() {
  char* buffer = format(NULL, 0, "Gigi", 1, 5);
  printf("%s\n", buffer);
  free(buffer);
}