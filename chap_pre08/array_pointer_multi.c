#include<stdio.h>

int main() {
  int matrix[2][5] = {{1,2,3,4,5}, {6,7,8,9,10}};

  for (int i=0; i<2; i++) {
    for (int j=0; j<5; j++) {
      printf("matrix[%d][%d] Adress: %p, Value: %d\n", i, j, &matrix[i][j], matrix[i][j]);
    }
  }

  printf("\n---\n\n");

  int (*pmatrix)[5] = matrix;
  for (int i=0; i<2; i++) {
    for (int j=0; j<5; j++) {
      printf("pmatrix[%d][%d] Adress: %p, Value: %d\n", i, j, &pmatrix[i][j], pmatrix[i][j]);
    }
  }
}