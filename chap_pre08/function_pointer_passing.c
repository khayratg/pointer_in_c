#include<stdio.h>

typedef int (*fptrOp) (int, int);

int add(int a, int b) { return a+b; }
int sub(int a, int b) { return a-b; }
int compute(fptrOp op, int a, int b) { return op(a, b); }

int main() {
  printf("%d\n", compute(add,5,6));
  printf("%d\n", compute(sub,5,6));
}


