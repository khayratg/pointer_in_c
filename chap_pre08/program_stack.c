#include <stdio.h>

float average(int* arr, int size) {
  int sum = 0;
  printf("arr: %p\n", &arr);
  printf("size: %p\n", &size);
  printf("sum: %p\n", &sum);

  for (int i=0; i<size; i++) {
    sum += arr[i];
  }

  return (sum * 1.0f) / size;
}

int main() {
  int A[] = {1,2,3,4};
  float av = average(A, 4);
  printf("A: Addr: %p Value: %p\n", &A, A);
  printf("A[0]: Addr: %p Value: %d\n", &A[0], A[0]);
  printf("average: %f\n", av);
}