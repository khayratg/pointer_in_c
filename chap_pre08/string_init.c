#include<stdio.h>
#include<stdlib.h>

int main() {
  char* prefix = "+";
  printf("sizeof(prefix): %lu, %s\n", sizeof(prefix), prefix);
  printf("sizeof(*prefix): %lu\n", sizeof(*prefix));
  printf("sizeof(*(prefix+1)): %lu\n", sizeof(*(prefix + 1)));

  prefix = (char*) malloc(2);
  *prefix = '+';
  *(prefix + 1) = 0;
  printf("sizeof(prefix): %lu, %s\n", sizeof(prefix), prefix);
  printf("sizeof(*prefix): %lu\n", sizeof(*prefix));
  printf("sizeof(*(prefix+1)): %lu\n", sizeof(*(prefix + 1)));

}