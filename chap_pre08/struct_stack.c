#include "struct_linked_list.h"

typedef LinkedList Stack;

void initializeStack(Stack* stack) {
  initializeList(stack);
}

void push(Stack* stack, void* data) {
  addHead(stack, data);
}

void* pop(Stack* stack) {
  Node* node;
  void* data;
  /* empty stack */
  if (stack->head == NULL) {
    data = NULL;
  }
  /* singel element */
  else if (stack->head->next == NULL) {
    node = stack->head;
    data = node->data;
    stack->head = stack->tail =  NULL;
    free(node);
  }
  /* multiple elements*/
  else {
    Node* head = stack->head;
    data = head->data;
    stack->head = stack->head->next;
    free(head);
  }

  return data;
}

int main() {
  Stack stack;

  Employee* bob = (Employee*) malloc(sizeof(Employee));
  strcpy(bob->name, "Bob");
  bob->age = 10;

  Employee* alice = (Employee*) malloc(sizeof(Employee));
  strcpy(alice->name, "Alice");
  alice->age = 30;

  Employee* susen = (Employee*) malloc(sizeof(Employee));
  strcpy(susen->name, "Susen");
  susen->age = 15;

  initializeStack(&stack);

  push(&stack, bob);
  printf("Pushed: %s\n", bob->name);
  push(&stack, alice);
  printf("Pushed: %s\n", alice->name);
  push(&stack, susen);
  printf("Pushed: %s\n", susen->name);

  void* data;
  for (int i=0; i<4; i++) {
    data = pop(&stack);
    printf("Poped: %s\n", ((Employee*) data)->name);
  }
}
