#include<stdio.h>
#include<stdlib.h>

size_t stringLength(char* str) {
  size_t length = 0;

  while(*(str++)) {
    printf("'%c'\n", *str);
    length++;
  }

  return length;
}

int main() {
  char* str = "hallo";

  printf("stringLength(\"%s\"): %lu\n", str, stringLength(str));
}