#include<stdlib.h>
#include<stdio.h>

int* allocateArray(int size, int value) {
  int* arr = (int*) malloc(size * sizeof(int));
  
  for (int i=0; i<size; i++) {
    arr[i] = value;
  }

  printf("sizeof(int): %zu\n", sizeof(int));
  printf("sizeof(int*): %zu\n", sizeof(int*));
  printf("size: %p\n", &size);
  printf("value: %p\n", &value);
  printf("arr: %p, %p\n", &arr, arr);
  return arr;
}

int main() {
  int elems = 5;
  int value = 10;
  int* vector;
  printf("elems: %p\n", &elems);
  printf("value: %p\n", &value);
  printf("vector: %p, %p\n", &vector, vector);
  vector = allocateArray(elems, value);
  printf("vector: %p, %p\n", &vector, vector);

  for (int i=0; i<elems; i++) {
    printf("%d\n", vector[i]);
  }
}