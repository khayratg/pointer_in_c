#include "struct_employee.h"
#include <stdlib.h>

typedef void (*DISPLAY) (void*);
typedef int (*COMPARE) (void*, void*);

typedef struct _node Node;

struct _node {
  void* data;
  Node* next;
};

typedef struct _linkedList {
  Node* head;
  Node* tail;
  Node* currenct;
} LinkedList;

void initializeList(LinkedList* list) {
  list->head = NULL;
  list->tail = NULL;
  list->currenct = NULL;
}

void addHead(LinkedList* list, void* data) {
  Node* node = (Node*) malloc(sizeof(Node));
  node->data = data;
  if (list->head == NULL) {
    list->tail = node;
    node->next = NULL;
  }
  else {
    node->next = list->head;
  }
  list->head = node;
}

void addTail(LinkedList* list, void* data) {
  Node* node = (Node*) malloc(sizeof(Node));
  node->data = data;
  node->next = NULL;

  if (list->head == NULL) {
    list->head = node;
  } else {
    list->tail->next = node;
  }
  list->tail = node;
}

void displayLinkdedList(LinkedList* list, DISPLAY display) {
  printf("\nLinked List\n");
  Node* current = list->head;
  while (current != NULL) {
    display(current->data);
    current = current->next;
  }
}

Node* getNode(LinkedList* list, COMPARE compare, void* data) {
  Node* current = list->head;

  while(current != NULL) {
    if (compare(current->data, data) == 0) {
      return current;
    }
    current = current->next;
  }
  return NULL;
}

void deleteNode(LinkedList* list, Node* node) {
  if (node == NULL) return;

  if (list->head == node) {
    if (list->head->next == NULL) {
      list->head = list->tail = NULL;
    } else {
      list->head = list->head->next;
    }
  } else {
    Node* tmp = list->head;
    while (tmp != NULL && tmp->next != node) {
      tmp = tmp->next;
    }
    if (tmp != NULL) {
      tmp->next = node->next;
    }
    free(node);
  }
}
