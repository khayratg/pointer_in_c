#include<stdio.h>

void one_dim_array() {
  int vector[5] = {1,2,3,4,5};
  printf("\n-------one-dim-------\n\n");
  printf("sizeof(int): %lu\n", sizeof(int));
  printf("array-size: %lu\n", sizeof(vector));
  printf("elems in array: %lu\n", sizeof(vector) / sizeof(int));
  printf("vector: %p\n", vector);
  printf("&vector: %p\n", &vector);
  for (int i=0; i<5; i++) {
    printf("&vector[%d]: %p, vector[%d]: %d\n", i, &vector[i], i, vector[i]);
  }

  int* pv = &vector;
  printf("pv (&vector): %p\n", pv);
  pv = vector;
  printf("pv (vector): %p\n", pv);
  pv = &vector[0];
  printf("pv (&vector[0]): %p\n", pv);

  printf("sizeof(vector): %lu, sizeof(*p): %lu\n", sizeof(vector), sizeof(*pv));
  for (int i=0; i<5; i++) {
    printf("*(pv + %d): %d\n", i, *(pv + i));
  }
}

void two_dim_array() {
  printf("\n-------two-dim-------\n\n");

  int matrix[2][3] = {
    {1,2,3},
    {4,5,6}
  };

  printf("sizeof(matrix): %lu\n", sizeof(matrix));
  printf("matrix: %p\n", matrix);
  printf("&matrix: %p\n", &matrix);
  for (int row=0; row<2; row++) {
      printf("&matrix[%d]: %p, sizeof(matrix[%d]): %lu\n", row,  &matrix[row], row, sizeof(matrix[row]));
    for (int col=0; col<3; col++) {
      printf("&matrix[%d][%d]: %p, val: %d\n", row, col, 
        &matrix[row][col], matrix[row][col]);
    }
  }
}

void multi_dim_array() {
  printf("\n-------multi-dim-------\n\n");

  int arr3d[3][2][4] = {
    {{ 1, 2 ,3, 4}, { 5, 6, 7, 8}},
    {{ 9,10,11,12}, {13,14,15,16}},
    {{17,18,19,20}, {21,22,23,24}}
  };

  printf("sizeof(arr3d): %lu\n", sizeof(arr3d));
}

int main() {
  one_dim_array();
  two_dim_array();
  multi_dim_array();
}