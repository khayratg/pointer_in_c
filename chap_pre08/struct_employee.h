#include<string.h>
#include<stdio.h>

typedef struct _employee {
  char name[32];
  unsigned char age;
} Employee;

int compareEmployee(Employee* e1, Employee* e2) {
  return strcmp(e1->name, e2->name);
}

void displayEmployee(Employee* e) {
  printf("%s\t%d\n", e->name, e->age);
}
