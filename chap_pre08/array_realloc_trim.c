#include<stdlib.h>
#include<stdio.h>
#include<string.h>

char* trim(char* phrase) {
  char* old = phrase;
  char* new = phrase;

  while(*old == ' ') { old++; }
  while(*old)        { *(new++) = *(old++); }
  *new = 0;

  return (char*) realloc(phrase, strlen(phrase)+1);
}

int main() {
  char* buffer = (char*) malloc(sizeof("  cat") + 1);
  strcpy(buffer, "  cat");
  printf("before: '%s'\n", buffer);
  buffer = trim(buffer);
  printf("after: '%s'\n", buffer);
}