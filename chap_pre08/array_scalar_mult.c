#include<stdio.h>

void scalarMult(int* vec, int n, int scalar) {
  printf("vec: %p\n", vec);
  printf("*vec: %d\n", *vec);
  for (int i = 0; i<n; i++) {
    *vec++ *= scalar;
  }
}

void printVec(int* vec, int n) {
  for (int i=0; i<n; i++) {
    printf("v[%d] = %d\n", i, vec[i]);
  }
}

int main() {
  int vector[3] = {1,2,3};
  int n = sizeof(vector) / sizeof(int);

  printf("vector: %p\n", vector);

  printVec(vector, n);
  scalarMult(vector, n, 5);
  printVec(vector, n);
  
}