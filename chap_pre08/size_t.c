#include <stdio.h>

int main() {
  size_t sizet = -1;

  printf("%d\n", sizet);
  printf("%zu\n", sizet);
  printf("%u\n", sizet);
  printf("%lu\n", sizet);

  printf("Size of *char: %lu\n", sizeof(char*));
  printf("Size of *int: %lu\n", sizeof(int*));
  printf("Size of *long: %lu\n", sizeof(long*));
  printf("Size of *short: %lu\n", sizeof(short*));
  printf("Size of short: %lu\n", sizeof(short));
  printf("Size of char: %lu\n", sizeof(char));
  printf("Size of int: %lu\n", sizeof(int));
  printf("Size of unsigned int: %lu\n", sizeof(unsigned int));
  printf("Size of long: %lu\n", sizeof(long));
  printf("Size of unsigned long: %lu\n", sizeof(unsigned long));
  printf("Size of void: %lu\n", sizeof(void));
}