#include<stdio.h>
#include<stdlib.h>

void displayArray(int** arr, int size) {
  for (int i=0; i<size; i++) {
    printf("%d, addr: %p, addr(arr[%d]): %p\n", **(arr + i), *(arr + i), i, arr + i);
    printf("%d\n", arr[i][0]);
  }
}

int main() {
  int* arr[5]; /* array of pointer to int */

  printf("sizeof(arr): %lu\n", sizeof(arr));
  printf("&arr: %p\n", &arr);
  printf("arr: %p\n", arr);
  for (int i=0; i<5; i++) {
    printf("&arr[%d]: %p, arr[%d]: %p\n", i, &arr[i], i, arr[i]);

    arr[i] = (int*) malloc(sizeof(int));

    printf("&arr[%d]: %p, arr[%d]: %p, *arr[%d]: %d\n", i, &arr[i], i, arr[i], i, *arr[i]);

    *arr[i] = i;

    printf("&arr[%d]: %p, arr[%d]: %p, *arr[%d]: %d\n", i, &arr[i], i, arr[i], i, *arr[i]);
  }

  printf("\n-----\n\n");

  for (int i=0; i<5; i++) {
    *(arr + i) = (int*) malloc(sizeof(int));

    printf("&arr[%d]: %p, arr[%d]: %p, *arr[%d]: %d\n", i, &arr[i], i, arr[i], i, *arr[i]);

    **(arr + i) = i;

    printf("&arr[%d]: %p, arr[%d]: %p, *arr[%d]: %d\n", i, &arr[i], i, arr[i], i, *arr[i]);
  }

  displayArray(arr, sizeof(arr) / sizeof(int*));
}