#include<stdio.h>
#include<string.h>
#include<stdlib.h>

typedef int (fptrOp) (const char*, const char*);

char toLower(char c) {
  int offset = 'a' - 'A';

  if (c < 'a') {
    c = c + offset;
  }
  return c;
}

char* stringToLower(const char* s) {
  char* result = (char*) malloc(strlen(s) + 1);
  char* current = result;

  while(*s) {
    *current++ = toLower(*s++);
  }
  *current = '\0';
  return result;
}

int compare(const char* s1, const char* s2) {
  return strcmp(s1, s2);
}

int compareIgnoreCase(const char* s1, const char* s2) {
  char* t1 = stringToLower(s1);
  char* t2 = stringToLower(s2);
  int result = strcmp(t1, t2);
  free(t1);
  free(t2);
  return result;
}

void sort(char* array[], size_t size, fptrOp cmp) {
  int swap = 1;
  while(swap == 1) {
    swap = 0;
    for (int i = 0; i < size - 1; i++) {
      if (cmp(array[i], array[i+1]) > 0) {
        swap = 1;
        char* tmp = array[i];
        array[i] = array[i+1];
        array[i+1] = tmp;
      }
    }
  }
}

void displayArray(char* array[], size_t size) {
  for (int i = 0; i < size; i++) {
    printf("'%s' ", array[i]);
  }
  printf("\n");
}

int main() {
  char* names[] = {"Otto", "Klaus", "alfons", "bob", "Adolf"};
  size_t size = sizeof(names) / sizeof(char*);
  displayArray(names, size);
  sort(names, size, compare);
  displayArray(names, size);
  sort(names, size, compareIgnoreCase);
  displayArray(names, size);
}