#include<stdio.h>

void display2DArrayUnknownSize(int* arr, int rows, int cols) {
  for (int r=0; r<rows; r++) {
    for (int c=0; c<cols; c++) {
      printf("%d ", *(arr + r*cols + c));
    }
    printf("\n");
  }
}

int main() {
  int matrix[2][5] = {
    {1,2,3,4,5},
    {6,7,8,9,10}
  };

  printf("%d\n", matrix[0][0]);
  printf("%lu, %lu\n", sizeof(1), sizeof(matrix[0][0]));
  printf("sizeof(int*): %lu\n", sizeof(int*));
  printf("sizeof(matrix): %lu, sizeof(matrix[0]): %lu, sizeof(matrix[0][0]): %lu\n", 
    sizeof(matrix), sizeof(matrix[0]), sizeof(matrix[0][0]));

  int rows = sizeof(matrix) / sizeof(matrix[0]);
  int cols = sizeof(matrix[0]) / sizeof(int);
  printf("rows: %d, cols: %d\n", rows, cols);

  display2DArrayUnknownSize(&matrix[0][0], rows, cols);
  /*
  display2DArrayUnknownSize(matrix, rows, cols);
  */
}