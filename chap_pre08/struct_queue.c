#include "struct_linked_list.h"

typedef LinkedList Queue;

void initializeQueue(Queue* queue) {
  initializeList(queue);
}

void enqueue(Queue* queue, void* data) {
  addHead(queue, data);
}

void* dequeue(Queue* queue) {
  Node* node;
  void* data;

  /* empty queue */
  if (queue->head == NULL) {
    data = NULL;
  }
  /* single node queue */
  else if (queue->head->next == NULL) {
    node = queue->head;
    data = node->data;
    queue->head = queue->tail = NULL;
    free(node);
  }
  /* multiple node queue */
  else {
    node = queue->head;
    while (node->next != queue->tail) {
      node = node->next;
    }
    Node* tail = queue->tail;
    queue->tail = node;
    node->next = NULL;
    data = tail->data;
    free(tail);
  }
  return data;
}

int main() {
  Employee* bob = (Employee*) malloc(sizeof(Employee));
  strcpy(bob->name, "Bob");
  bob->age = 10;

  Employee* alice = (Employee*) malloc(sizeof(Employee));
  strcpy(alice->name, "Alice");
  alice->age = 30;

  Employee* susen = (Employee*) malloc(sizeof(Employee));
  strcpy(susen->name, "Susen");
  susen->age = 15;

  Queue queue;
  initializeQueue(&queue);

  enqueue(&queue, bob);
  printf("Enqueued: %s\n", bob->name);
  enqueue(&queue, alice);
  printf("Enqueued: %s\n", alice->name);
  enqueue(&queue, susen);
  printf("Enqueued: %s\n", susen->name);

  void* data;
  for (int i=0; i<4; i++) {
    data = dequeue(&queue);
    printf("Dequeued: %s\n", ((Employee*) data)->name);
  }
}
