#include <stdio.h>

int main() {
  const int limit = 500;
  int num = 100;

  int* const pci = &num;
  printf("pci Adress: %lu, value: %lu\n", &pci, pci);
  printf("num Adress: %lu, value: %d\n", &num, num);
  
  *pci = 200;
  printf("pci Adress: %lu, value: %lu\n", &pci, pci);
  printf("num Adress: %lu, value: %lu\n", &num, num);

  int* const pci2 = &limit;
  printf("pci2 Adress: %lu, value: %lu, deref: %d\n", &pci2, pci2, *pci2);
  printf("limit Adress: %lu, value: %d\n", &limit, limit);

  *pci2 = 501;
  printf("pci2 Adress: %lu, value: %lu, deref: %d\n", &pci2, pci2, *pci2);
  printf("limit Adress: %lu, value:%d\n", &limit, limit);
}