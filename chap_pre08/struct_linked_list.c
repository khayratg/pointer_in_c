#include "struct_employee.h"
#include <stdlib.h>

typedef void (*DISPLAY) (void*);
typedef int (*COMPARE) (void*, void*);

typedef struct _node Node;

struct _node {
  void* data;
  Node* next;
};

typedef struct _linkedList {
  Node* head;
  Node* tail;
  Node* currenct;
} LinkedList;

void initializeList(LinkedList* list) {
  list->head = NULL;
  list->tail = NULL;
  list->currenct = NULL;
}

void addHead(LinkedList* list, void* data) {
  Node* node = (Node*) malloc(sizeof(Node));
  node->data = data;
  if (list->head == NULL) {
    list->tail = node;
    node->next = NULL;
  }
  else {
    node->next = list->head;
  }
  list->head = node;
}

void addTail(LinkedList* list, void* data) {
  Node* node = (Node*) malloc(sizeof(Node));
  node->data = data;
  node->next = NULL;

  if (list->head == NULL) {
    list->head = node;
  } else {
    list->tail->next = node;
  }
  list->tail = node;
}

void displayLinkdedList(LinkedList* list, DISPLAY display) {
  printf("\nLinked List\n");
  Node* current = list->head;
  while (current != NULL) {
    display(current->data);
    current = current->next;
  }
}

Node* getNode(LinkedList* list, COMPARE compare, void* data) {
  Node* current = list->head;

  while(current != NULL) {
    if (compare(current->data, data) == 0) {
      return current;
    }
    current = current->next;
  }
  return NULL;
}

void deleteNode(LinkedList* list, Node* node) {
  if (node == NULL) return;

  if (list->head == node) {
    if (list->head->next == NULL) {
      list->head = list->tail = NULL;
    } else {
      list->head = list->head->next;
    }
  } else {
    Node* tmp = list->head;
    while (tmp != NULL && tmp->next != node) {
      tmp = tmp->next;
    }
    if (tmp != NULL) {
      tmp->next = node->next;
    }
    free(node);
  }
}

int main() {
  Employee* bob = (Employee*) malloc(sizeof(Employee));
  strcpy(bob->name, "Bob");
  bob->age = 10;

  Employee* alice = (Employee*) malloc(sizeof(Employee));
  strcpy(alice->name, "Alice");
  alice->age = 30;

  Employee* susen = (Employee*) malloc(sizeof(Employee));
  strcpy(susen->name, "Susen");
  susen->age = 15;

  LinkedList list;
  initializeList(&list);

  addHead(&list, bob);
  addHead(&list, alice);
  addHead(&list, susen);

  addTail(&list, susen);
  addTail(&list, alice);
  addTail(&list, bob);

  displayLinkdedList(&list, (DISPLAY) displayEmployee);

  COMPARE cmp = (COMPARE) compareEmployee;
  DISPLAY disp = (DISPLAY) displayEmployee;
  Node* node;
  node = getNode(&list, cmp, susen);
  deleteNode(&list, node);
  displayLinkdedList(&list, disp);

  deleteNode(&list, getNode(&list, cmp, susen));
  displayLinkdedList(&list, disp);

  node = getNode(&list, (COMPARE) compareEmployee, susen);
  deleteNode(&list, node);
  displayLinkdedList(&list, (DISPLAY) displayEmployee);
}
