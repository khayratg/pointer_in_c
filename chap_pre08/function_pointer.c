#include<stdio.h>

typedef int (*funcptr) (int);

int square(int num) {
  return num * num;
}

int main() {
  int n = 5;
  int (*fptr1) (int);
  fptr1 = square;
  printf("%d sqaured: %d\n", n, fptr1(n));

  funcptr fptr2 = square;
  printf("%d sqaured: %d\n", n, fptr2(n));
}