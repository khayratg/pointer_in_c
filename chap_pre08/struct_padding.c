#include<stdio.h>

typedef struct _a {
  short a;
} A;

typedef struct _b {
  int b;
} B;

typedef struct _c {
  char* c;
} C;

typedef struct _ab {
  short a;
  int b;
} AB;

typedef struct _ba {
  int b;
  short a;
} BA;

typedef struct _ac {
  short a;
  char* c;
} AC;

typedef struct _ca {
  char* c;
  short a;
} CA;

typedef struct _bc {
  int b;
  char* c;
} BC;

typedef struct _cb {
  char* c;
  int b;
} CB;

typedef struct _abc {
  short a;
  int b;
  char* c;
} ABC;

typedef struct _bac {
  short a;
  int b;
  char* c;
} BAC;

typedef struct _bca {
  int b;
  char* c;
  short a;
} BCA;

typedef struct _cba {
  char* c;
  int b;
  short a;
} CBA;

typedef struct _cab {
  char* c;
  short a;
  int b;
} CAB;

int main() {
  printf("sizeof(short): %lu\n", sizeof(short));
  printf("sizeof(int): %lu\n", sizeof(int));
  printf("sizeof(char*): %lu\n", sizeof(char*));
  printf("\n");
  printf("sizeof(A): %lu\n", sizeof(A));
  printf("sizeof(B): %lu\n", sizeof(B));
  printf("sizeof(C): %lu\n", sizeof(C));
  printf("\n");
  AB ab;
  printf("sizeof(AB): %lu, &AB: %p, &AB.a: %p, &AB.b: %p\n", sizeof(AB), &ab, &ab.a, &ab.b);
  BA ba;
  printf("sizeof(BA): %lu, &BA: %p, &BA.b: %p, &BA.a: %p\n", sizeof(BA), &ba, &ba.b, &ba.a);
  printf("sizeof(AC): %lu\n", sizeof(AC));
  printf("sizeof(BC): %lu\n", sizeof(BC));
  printf("sizeof(ABC): %lu\n", sizeof(ABC));
  printf("sizeof(BAC): %lu\n", sizeof(BAC));
  BCA bca;
  printf("sizeof(BCA): %lu, &BCA: %p, &BCA.b: %p, &BCA.c: %p, &BCA.a: %p\n", sizeof(BCA), &bca, &bca.b, &bca.c, &bca.a);
  printf("sizeof(CBA): %lu\n", sizeof(CBA));
  printf("sizeof(CAB): %lu\n", sizeof(CAB));
}