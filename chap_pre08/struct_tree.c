#include<stdio.h>
#include<stdlib.h>
#include"struct_employee.h"

typedef int (*COMPARE) (void*, void*);
typedef void (*DISPLAY) (void*);

typedef struct _tree TreeNode;
struct _tree {
  void* data;
  TreeNode* left;
  TreeNode* right;
};

void insertNode(TreeNode** root, COMPARE comp, void* data) {
  TreeNode* node = (TreeNode*) malloc(sizeof(TreeNode));
  node->data = data;
  node->left = NULL;
  node->right = NULL;

  if (*root == NULL) {
    *root = node;
    return;
  }

  while (1) {
    /* new node is less */
    if (comp((*root)->data, data) > 0) {
      if ((*root)->left == NULL) {
        (*root)->left = node;
        break;
      } else {
        *root = (*root)->left;
      }
    }
    /* new node is greater or equal */
    else {
      if ((*root)->right == NULL) {
        (*root)->right = node;
        break;
      } else {
        *root = (*root)->right;
      }
    }
  }
}

void inOrder(TreeNode* root, DISPLAY disp) {
  if (root != NULL) {
    inOrder(root->left, disp);
    disp(root->data);
    inOrder(root->right, disp);
  }
}

void postOrder(TreeNode* root, DISPLAY disp) {
  if (root != NULL) {
    postOrder(root->left, disp);
    postOrder(root->right, disp);
    disp(root->data);
  }
}

void preOrder(TreeNode* root, DISPLAY disp) {
  if (root != NULL) {
    disp(root->data);
    preOrder(root->left, disp);
    preOrder(root->right, disp);
  }
}

int main() {
  Employee* bob = (Employee*) malloc(sizeof(Employee));
  strcpy(bob->name, "Bob");
  bob->age = 10;

  Employee* alice = (Employee*) malloc(sizeof(Employee));
  strcpy(alice->name, "Alice");
  alice->age = 30;

  Employee* susen = (Employee*) malloc(sizeof(Employee));
  strcpy(susen->name, "Susen");
  susen->age = 15;

  TreeNode* tree = NULL;

  COMPARE cmp = (COMPARE) compareEmployee;
  insertNode(&tree, cmp, bob);
  printf("inserted bob\n");
  insertNode(&tree, cmp, susen);
  printf("inserted susen\n");
  insertNode(&tree, cmp, alice);
  printf("inserted alice\n");

  DISPLAY disp = (DISPLAY) displayEmployee;
  printf("\ninOrder\n");
  inOrder(tree, disp);

  printf("\npostOrder\n");
  postOrder(tree, disp);

  printf("\npreOrder\n");
  preOrder(tree, disp);
}
