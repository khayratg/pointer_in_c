#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct _person {
  char* firstName;
  char* lastName;
  char* title;
  short age;
} Person;

#define LIST_SIZE 10
Person* list[LIST_SIZE];

void initializePerson(Person *person, const char* fn, const char* ln, const char* title, short age) {
  person->firstName = (char*) malloc(strlen(fn) + 1);
  strcpy(person->firstName, fn);
  person->lastName = (char*) malloc(strlen(ln) + 1);
  strcpy(person->lastName, ln);
  person->title = (char*) malloc(strlen(title) + 1);
  strcpy(person->title, title);
  person->age = age;
}

void deallocatePerson(Person* person) {
  free(person->firstName);
  free(person->lastName);
  free(person->title);
}

void displayPerson(Person* person) {
  printf("%s %s %s, Alter: %d\n", person->title, person->firstName, person->lastName, person->age);
}

void initializeList() {
  for (int i = 0; i < LIST_SIZE; i++) {
    list[i] = NULL;
  }
}

Person* getPerson() {
  for (int i = 0; i < LIST_SIZE; i++) {
    if (list[i] != NULL) {
      Person* person = list[i];
      list[i] = NULL;
      return person;
    }
  }
  return (Person*) malloc(sizeof(Person));
}

void returnPerson(Person* person) {
  for (int i = 0; i < LIST_SIZE; i++) {
    if (list[i] == NULL) {
      list[i] = person;
      return;
    }
  }
  deallocatePerson(person);
  free(person);
}

int main() {
  const int addOn = 10;
  Person* l[LIST_SIZE + addOn];

  initializeList();
  for (int i=0; i<LIST_SIZE + addOn; i++) {
    Person *person = getPerson();
    l[i] = person;
    initializePerson(person, "Klaus", "Allofs", "Dr.", 25+i);
    displayPerson(person);
  }
  for (int i=0; i<LIST_SIZE + addOn; i++) {
    returnPerson(l[i]);
  }
}