#include<stdio.h>

typedef int (*fptrOp) (int, int);

int add(int a, int b) { return a+b; }
int sub(int a, int b) { return a-b; }

fptrOp select(char opCode) {
  switch (opCode)
  {
  case '+':
    return add;
  case '-':
    return sub;
  }
}

int evaluate(char opCode, int a, int b) {
  return select(opCode)(a,b);
}

int main() {
  printf("%d\n", evaluate('+', 5, 6));
  printf("%d\n", evaluate('-', 5, 6));
}