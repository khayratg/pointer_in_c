#include<stdlib.h>
#include<stdio.h>

void allocateArray(int** arr, int size, int value) {
  printf("arr: %p, %p, %p\n", &arr, arr, *arr);
  *arr = (int*) malloc(size * sizeof(int));
  printf("arr: %p, %p, %p\n", &arr, arr, *arr);

  for (int i=0; i<size; i++) {
    (*arr)[i] = value;
  }
}

int main() {
  int* vector;
  printf("vector: %p, %p\n", &vector, vector);
  allocateArray(&vector, 5, 5);
  printf("vector: %p, %p\n", &vector, vector);

  for (int i=0; i<5; i++) {
    printf("%d\n", vector[i]);
  }
}