#include<stdio.h>

int main() {
  float num = 3.25f;
  //float num = 0.0f;
  unsigned int* ptrValue = (unsigned int*) &num;
  unsigned int result = (*ptrValue & 0x80000000) == 0;

  printf("%p, %u\n", ptrValue, *ptrValue);
  printf("result: %d\n", result);
}
