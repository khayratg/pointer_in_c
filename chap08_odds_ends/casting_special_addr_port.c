#include<stdio.h>
#include<string.h>

#define PORT 0xB0000000

int main() {
  unsigned int* volatile const port = (unsigned int*) PORT;

  // read from the port
  int value = *port;
  printf("read..\n");

  // write to the port
  *port = 0x0BF4;
  printf("wrote..\n");
}
