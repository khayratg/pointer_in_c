#include<stdio.h>
#include<string.h>

int main() {
  int* ptr;
  int num = 5;
  printf("%p\n", &num);
  printf("%d\n", num);
  printf("%d\n", *ptr);
  printf("%p\n", ptr);
  printf("%p\n", &ptr);

  ptr = &num;
  printf("2.\n");
  printf("%d\n", *ptr);
  printf("%p\n", ptr);
  printf("%p\n", &ptr);

  memset((void*)ptr, 0, sizeof(ptr));
  printf("3.\n");
  printf("%p\n", &num);
  printf("%d\n", num);
  printf("%p\n", ptr);
  printf("%p\n", &ptr);
  //printf("%d\n", *ptr);

  memset((void*)&ptr, 0, sizeof(ptr));
  printf("4.\n");
  printf("%p\n", ptr);
  printf("%p\n", &ptr);
  //printf("%d\n", *ptr);

}
